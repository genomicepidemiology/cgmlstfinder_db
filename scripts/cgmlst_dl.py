import urllib.request
import sys, os, json, gzip, shutil
import argparse
import urllib.request
import subprocess
import pickle

def is_gzipped(file_path):
    ''' Returns True if file is gzipped and False otherwise.
        The result is inferred from the first two bits in the file read
        from the input path.
        On unix systems this should be: 1f 8b
        Theoretically there could be exceptions to this test but it is
        unlikely and impossible if the input files are otherwise expected
        to be encoded in utf-8.
    '''
    with open(file_path, mode='rb') as fh:
        bit_start = fh.read(2)
    if(bit_start == b'\x1f\x8b'):
        return True
    else:
        return False

def gzip_file(filename, copy=False):
    if not is_gzipped(filename):
        with open(filename, 'rb') as f_in, gzip.open(filename + '.gz', 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
        if not copy:
            os.remove(filename)
    else:
        os.rename(filename, filename + '.gz')
    filename = filename + '.gz'
    return filename

def request_data(url, json_output=False, filename=None):
    try:
        if filename:
            urllib.request.urlretrieve(url, filename) 
        else:
            response = urllib.request.urlopen(url)
    except urllib.request.HTTPError as Response_error:
        sys.exit('{} {}. <{}>\n Reason: {}'.format(Response_error.code, Response_error.msg,
                                           Response_error.geturl(), Response_error.read()))
    if filename:
        return 
    if json_output:
        data = json.loads(response.read().decode(response.info().get_param('charset') or 'utf-8'))
    else:
        data = response.read()
    return data

def run_cmd(cmd):
    process = subprocess.Popen(cmd, stderr=subprocess.PIPE,
                               stdout=subprocess.PIPE, shell=True)
    out, err = process.communicate()
    out = out.decode('utf-8')
    err = err.decode('utf-8')

    if process.returncode != 0:
        err_stream = err.split("\n")
        err = []
        for line in err_stream:
            if not line.startswith("#"):
                err.append(line)

        sys.exit(("Process returned non-zero error code!" +
                  "Cmd: {}\nError code: {}\n{}").format(cmd, process.returncode, err))

    return out, err

def creat_profile_dict(profile_filename, pickle_path):
    profile_file = gzip.open(profile_filename, "rb")
    loci_list = profile_file.readline().rstrip().split(b"\t")[1:]

    # Create dict for looking up st-types with locus and allel combinations
    loci_allel_dict = {}
    # For each loci initate an inner dictionary to store allel and st-types key-value pairs
    for locus in loci_list:
        loci_allel_dict[locus.decode("utf-8")] = {}

    count_line = 0    
    for line in profile_file:
        profile = line.strip().split(b"\t") 
        st_name = profile[0].decode("utf-8")
        allel_list = profile[1:]
        count_line += 1
        if count_line%15000 == 0:
            print("{} st-types have been stored in the dict".format(count_line))
        
        # Go through each ST-types loci allel combination and save it with the st-type in dict
        for i in range(len(allel_list)):
            allel = allel_list[i].decode("utf-8")
            locus = loci_list[i].decode("utf-8")
            if allel in loci_allel_dict[locus]:
                loci_allel_dict[locus][allel] += [st_name] #loci_allel_dict[locus][allel].append(st_name)
            else:
                loci_allel_dict[locus][allel] = [st_name]

    profile_file.close()

    with open(pickle_path, "wb" ) as p:
        pickle.dump(loci_allel_dict, p)

# Parsing input arguments
parser = argparse.ArgumentParser()
parser.add_argument('-o','--outdir', help="output directory", required=True)
parser.add_argument('-db','--database_dir', help="output directory", required=True)
parser.add_argument('-k','--kma_index', help="output directory", required=True)

args = parser.parse_args()

outdir = os.path.abspath(args.outdir)
if not os.path.isdir(outdir):
    sys.exit("Output directory '{}' does not exist.".format(outdir))

db_dir = os.path.abspath(args.database_dir)
if not os.path.isdir(db_dir):
    sys.exit("Database directory '{}' does not exist.".format(db_dir))

#TODO get species from config
schemes = {"salmonella":   {"profile_url":"http://enterobase.warwick.ac.uk/schemes/Salmonella.cgMLSTv2/profiles.list.gz",
                            "fasta_url":"http://enterobase.warwick.ac.uk/schemes/Salmonella.cgMLSTv2/{}.fasta.gz",
                            "name": "Salmonella",
                            "host": "Enterobase"},
           "ecoli":        {"profile_url":"http://enterobase.warwick.ac.uk/schemes/Escherichia.cgMLSTv1/profiles.list.gz",
                            "fasta_url":"http://enterobase.warwick.ac.uk/schemes/Escherichia.cgMLSTv1/{}.fasta.gz",
                            "name": "Escherichia coli",
                            "host": "Enterobase"},
           "yersinia":     {"profile_url":"http://enterobase.warwick.ac.uk/schemes/Yersinia.cgMLSTv1/profiles.list.gz",
                            "fasta_url":"http://enterobase.warwick.ac.uk/schemes/Yersinia.cgMLSTv1/{}.fasta.gz",
                            "name": "Yersinia",
                            "host": "Enterobase"},
           "clostridium":  {"profile_url":"http://enterobase.warwick.ac.uk/schemes/clostridium.cgMLSTv1/profiles.list.gz",
                            "fasta_url":"http://enterobase.warwick.ac.uk/schemes/clostridium.cgMLSTv1/{}.fasta.gz",
                            "name": "Clostridium",
                            "host": "Enterobase"},
           "campylobacter":{"profile_url": "http://rest.pubmlst.org/db/pubmlst_campylobacter_seqdef/schemes/4/profiles_csv",
                            "fasta_url":"http://rest.pubmlst.org/db/pubmlst_campylobacter_seqdef/loci/{}/alleles_fasta",
                            "name":"Campylobacter",
                            "host":"PubMLST"},
          }
scheme_salm = {"salmonella":   {"profile_url":"http://enterobase.warwick.ac.uk/schemes/Salmonella.cgMLSTv2/profiles.list.gz",
                            "fasta_url":"http://enterobase.warwick.ac.uk/schemes/Salmonella.cgMLSTv2/{}.fasta.gz",
                            "name": "Salmonella",
                            "host": "Enterobase"}}


for species in schemes:
    print("Downlaoding scheme for", species)
#    if species != "clostridium":
#        continue
    os.makedirs(os.path.join(outdir, species), mode=0o775, exist_ok=True)
    os.makedirs(os.path.join(db_dir, species), mode=0o775, exist_ok=True)
    url = schemes[species]["profile_url"]

    profile_file = os.path.join(outdir, species, "profiles.tsv")

    # For all other databases than listeria download profiles
    if schemes[species]["profile_url"] != "":
        # Download profiles
        request_data(url, filename=profile_file)

        # Gzip profile file if not already compressed
        profile_file = gzip_file(profile_file)

    else:
        profile_file = gzip_file(profile_file, copy=True)

    # Write list of loci based on profile header
    with gzip.open(profile_file, 'rb') as infile:
        header = infile.readline()

    loci_list = header.decode("utf-8").rstrip().split("\t")[1:]

    # Create dict of locus/allele combiantions in differnet st types d[locus][allele] = [st1, st2, st3 ...]
    if schemes[species]["profile_url"] != "":
        pickle_path = os.path.join(db_dir, species, "profile.p")
        creat_profile_dict(profile_file, pickle_path)

    with open(os.path.join(db_dir, species, "loci_list.txt"), "w") as outfile:
        outfile.write("\n".join(loci_list))

    # Download each locus fasta sequence into one file
    fasta_file = os.path.join(outdir, species, "all_loci.fsa")
    fasta_out = open(fasta_file, "wb")
    for locus in loci_list:
        url = schemes[species]["fasta_url"].format(locus)
        data = request_data(url)
        fasta_out.write(data)
        
    fasta_out.close()

    # Gzip profile file if not already compressed
    fasta_file =  gzip_file(fasta_file)

    # Index data
    cmd = "{} -i {} -o {} -NI".format(args.kma_index, fasta_file, os.path.join(db_dir, species, species))
    print(cmd)
    our, err = run_cmd(cmd)

# Make config file
with open(os.path.join(db_dir, "config"), "w") as outfile:
    for species in schemes:
        outfile.write("{}\t{}\t{}\n".format(species, schemes[species]["name"], schemes[species]["host"]))
