cgMLSTFinder DATABASES - Scripts
===================

This part of the cgMLST bitbucket repository  is for scripts regarding the cgMLST database

It currently holds: 

- cgmlst_dl.py 

**cgmlst_dl.py** is used to download and index the database using KMA. To add additional databases add the urls to the schemes dictionary around line 120.
