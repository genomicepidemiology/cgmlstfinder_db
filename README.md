cgMLSTFinder DATABASES
===================

This bitbucket page is only kept for legacy purposes and no longer relevant. The cgMLST database can be downloaded directly and ready to use as a Tar archive [here](https://cge.food.dtu.dk/services/cgMLSTFinder/etc/cgmlst_db.tar.gz).

________________________________

This project documents the installation, validation and update of cgMLSTFinder databases.
The databases stored on our FTP site, are needed by the program cgMLSTFinder (https://bitbucket.org/genomicepidemiology/cgmlstfinder)


Documentation
=============

## Content of the repository

1. INSTALL.py      - the program for installing all databases or one specific database
2. README.md  

## Databases

* campy: Campylobacter jejuni
* ecoli: Escherichia coli
* salmonella: Salmonella
* clostridium: Clostridium
* yersinia: Yersinia 

## Installation - Clone repository
```bash
# Go to wanted location for cgMLSTFinder db 
cd /path/to/some/dir
# Clone and enter the cgMLSTFinder db directory
git clone https://bitbucket.org/genomicepidemiology/cgmlstfinder_db.git
cd cgmlstfinder_db
```

## Download and install cgMLSTFinder database(s)

The INSTALL program allows you to install either all available databases (campylobacter, ecoli, clostridium, salmonella, yersinia) or to download one specific database.
There is also an extra option to download database(s) from a specific date tag (update). If there is no date tag specified, database(s) will be downloaded automatically 
from the most current update. The INSTALL program downloads the binary files of the database(s).

usage: INSTALL.py [-h] [-o OUTDIR] [-t DATE_TAG] [-s SPECIES [SPECIES ...]]

optional arguments:
  -h, --help            show this help message and exit
  -o OUTDIR, --outdir OUTDIR
                        Output directory, if not specified it is the directory
                        where the INSTALL.py script is located
  -t DATE_TAG, --date_tag DATE_TAG
                        Date tag for the database to be downloaded
  -s SPECIES [SPECIES ...], --species SPECIES [SPECIES ...]
                        Species that should be downloaded

```bash
# Below we provide alternative example commands to download the database(s) you want.
#1. Install all cgMLSTFinder databases from latest update
python3 INSTALL.py
#2  Install all cgMLSTFinder databases from latest update to a specific path
python3 INSTALL.py -o /path/to/some/dir
#3. Install all cgMLSTFinder databases from specific date - you can find available updates in our FTP site.
python3 INSTALL.py -t 20180306
#4. Install only one cgMLSTFinder database from latest update, e.g. campylobacter
python3 INSTALL.py -s campylobacter
#4. Install only the campylobacter cgMLSTFinder database from specific date - you can find available updates in our FTP site.
pyhton3 INSTALL.sh -s campylobacter -t 20180306
```


## Update the cgMLSTFinder database(s)

The cgMLST databases are updated automaticly once a week. To stay in sync with this update the install script should be run every week. The updated databases are all available on the ftp-site


## Available databases
You can find all our available databases in our [FTP site](ftp://ftp.cbs.dtu.dk/public/CGE/databases/cgMLSTFinder/)

## Who do I talk to?
To get in contact with us, please contact cgehelp@cbs.dtu.dk
